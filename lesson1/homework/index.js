/*

  Задание - используя классы и (или) прототипы создать программу, которая будет
  распределять животных по зоопарку.
*/
const zones = {
      'mammals': [],
      'birds': [],
      'fishes': [],
      'reptile': [],
      'others': []
};

class Zoo {
    constructor(name) {
        this.name = name;
        this.animalCount = 0;
        this.zones = {...zones};
    };
    
    addAnimal(animalObj) {
      // Добавляет животное в зоопарк в нужную зону.
      // зона берется из класса который наследует Animal
      // если у животного нету свойства zone - то добавлять в others
      if (animalObj.zone) {
        for (let zone in this.zones) {
          if (zone === animalObj.zone) {
            this.zones[zone].push(animalObj);
            this.animalCount += 1;
            return;
          }
        }
        this.zone[others].push(animalObj);
        this.animalCount += 1;
      } else {
        this.zone[others].push(animalObj);
        this.animalCount += 1;
      }
    };

    removeAnimal(animalName) {
      // удаляет животное из зоопарка
      // поиск по имени
      for (let zone in this.zones) {
        this.zones[zone].forEach(item => {
          if (item.name === animalName) {
            this.zones[zone].pop(item);
            this.animalCount -= 1;
            return;
          }
        });
      }
    };

    getAnimal(type, value) {
      // выводит информацию о животном
      // поиск по имени или типу где type = name/type
      // а value значение выбраного типа поиска
      if (type === 'name')
        for (let zone in this.zones) {
          this.zones[zone].map(anim => {
            if ( anim.name === value )
              console.log(`{name: ${anim.name}, type: ${anim.type}, zone: ${anim.zone}}`)
          })
        }
      if (type === 'type')
        for (let zone in this.zones) {
          this.zones[zone].map(anim => {
            if ( anim.type === value )
              console.log(`{name: ${anim.name}, type: ${anim.type}, zone: ${anim.zone}}`)
          })
        }
    };

    countAnimals() {
      // функция считает кол-во всех животных во всех зонах
      // и выводит в консоль результат
      let count = 0;
      for (let zone in this.zones) {
        count += this.zones[zone].length
      }
      return count;
    };
};

//  Есть родительский класс Animal у которого есть методы и свойства:
class Animal {
    constructor(name, phrase, foodType = 'herbivore') {
        this.name = name; // имя животного для поиска
        this.phrase = phrase;
        this.foodType = (foodType === 'herbivore') ? 'herbivore' : 'carnivore';  // Травоядное или Плотоядное животное
    }
    
    eatSomething() {
        console.log('Hroom hroom!');
    }
}

/*
  //Дальше будут классы, которые расширяют класс Animal - это классы:
  - mammals
  - birds
  - fishes
  - pertile

  каждый из них имеет свои свойства и методы:
  в данном примере уникальными будут run/speed. У птиц будут методы fly & speed и т.д
*/

class Mammal extends Animal {
    constructor(name, phrase, foodType, type, speed) {
      super(name, phrase, foodType);
      this.zone = 'mammals';   // дублирует название зоны, ставиться по умолчанию
      this.type = type;     // что за животное
      this.speed = speed;
    }
    
    run() {
        console.log( `${this.type} Rex run with speed ${this.speed} km/h` );
    };
}

class Bird extends Animal {
    constructor(name, phrase, foodType, type, speed) {
      super(name, ' ', foodType);
      this.zone = 'birds';   // дублирует название зоны, ставиться по умолчанию
      this.type = type;     // что за животное
      this.speed = speed;
    }
    
    fly() {
        console.log( `${this.type} Rex flys with speed ${this.speed} km/h` );
    };
}

class Fish extends Animal {
    constructor(name, foodType, type, speed) {
      super(name, ' ', foodType);
      this.zone = 'fishes';   // дублирует название зоны, ставиться по умолчанию
      this.type = type;     // что за животное
      this.speed = speed;
    }
    
    swim() {
        console.log( `${this.type} Rex swims with speed ${this.speed} km/h` );
    };
}

class Reptile extends Animal {
    constructor(name, foodType, type, speed) {
      super(name, ' ', foodType);
      this.zone = 'reptile';   // дублирует название зоны, ставиться по умолчанию
      this.type = type;     // что за животное
      this.speed = speed;
    }
    
    crawl() {
        console.log( `${this.type} Rex crawls with speed ${this.speed} km/h` );
    };
}

//Тестирование:
    const centralzoo = new Zoo('name');
    var Rex = new Mammal('Rex', 'Wooof', 'herbivore', 'woolf', 15 );
    centralzoo.addAnimal(Rex);
    //   // Добавит в your_zooName.zones.mamals новое животное.
    var Dex = new Mammal('Dex', 'Wooof', 'herbivore', 'woolf', 11 );
    centralzoo.addAnimal(Dex);
    //   // Добавит в your_zooName.zones.mamals еще одно новое животное.

    centralzoo.getAnimal('name', 'Rex'); //-> {name:"Rex", type: 'wolf' ...}
    centralzoo.getAnimal('type', 'woolf'); //-> [{RexObj},{DexObj}];

    console.log(centralzoo.animalCount)
    console.log(centralzoo.countAnimals())
    centralzoo.removeAnimal('Rex');
    console.log(centralzoo.animalCount)
    console.log(centralzoo.countAnimals())


/*
    Программу можно расширить и сделать в виде мини игры с интерфейсом и сдать
    как курсовую работу!
    Идеи:
    - Добавить ранжирование на травоядных и хищников
    - добавив какую-то функцию которая иммитирует жизнь в зоопарке. Питание, движение, сон животных и т.д
    - Условия: Если хищник и травоядный попадает в одну зону, то хищник сьедает травоядное и оно удаляется из зоопарка.
    - Графическая оболочка под программу.
*/
