export class Post {
    constructor({id, link, name, description, image, likes = 0}) {
      Object.defineProperty( this, 'id', { 
				enumerable: false ,
				value: id
			});
      Object.defineProperty( this, 'link', { 
        value: link
      });
      Object.defineProperty( this, 'name', { 
        enumerable: true,
        writable: true,
				value: name
      });
      Object.defineProperty( this, 'description', { 
        enumerable: true,
        writable: true,
				value: description
      });
      Object.defineProperty( this, 'naimageme', { 
        enumerable: true,
        writable: true,
				value: image
      });
      if (+likes >= 0) {
        likes = +likes;
      } else {
        likes = 0;
      }
      Object.defineProperty( this, 'likes', { 
        enumerable: true,
        writable: true,
				value: likes
      });
      this.comments = [];
      // this.addLikes.bind(this);
      // this.addComment.bind(this);
    }

    get id() {
      return this.id;
    }

    get link() {
      return this.link;
    }

    get name() {
      return this.name;
    }

    set name(name) {
      this.name = name;
      console.log(this.name);
    }

    get description() {
      return this.description;
    }

    set description(desc) {
      this.description = desc;
      console.log(this.description);
    }

    get image() {
      return this.image;
    }

    set image(img) {
      this.image = img;
      console.log(this.image);
    }

    addLikes = (e) => {
      e.preventDefault();
      this.likes += 1;
      console.log(e.target)
      e.target.innerHTML = `Like ${this.likes}`;
    }

    addComment = (e) => {
      e.preventDefault();
      let parent = e.target.closest('.col');
      let value = parent.querySelector('input').value;
      this.comments.push(value);
      parent = parent.closest('.post');
      this.render(parent);
      console.log(this.comments);
    }

    render(target) {
      let innerHTML = `
          <div class='col-2'>
            <img src='${this.image}'>
          </div>
          <div class='col'>
            <a href="${this.link}">
              <h4>${this.name}</h4>
            </a>
            <p>${this.description}</p>
            <div class='row'>
              
              <button class='btn like' data-id="${this.id}">Like ${this.likes}</button>
              <div class="col">
      `
      if (this.comments.length > 0) {
        innerHTML += `
          <h5>Comments (${this.comments.length})<h5>
        `
        this.comments.forEach(item => {
          innerHTML += `<p id="">${item}</p>`
        });
        }
      innerHTML += `
                <input type="text" placeholder="Write a comment">
                <button class='btn comment'>Add Comment</button>
              </div>
            </div>
          </div>
      `
      target.innerHTML = innerHTML;
      let likebtn = target.querySelector('.like')
      likebtn.addEventListener('click', this.addLikes)
      target.querySelector('.comment').addEventListener('click', this.addComment)
    }
  }

  export class EdvartPost extends Post {
    constructor(...args) {
      super(args)
    }
    
    buyItem = (e) => {
      e.preventDefault();
      console.log(`Yuo had bought ${this.name}`);
      e.target.innerHTML = `Yuo had bought ${this.name}`;
    }

    render(target) {
      let innerHTML = `
            <div class='col-2'>
              <img src='${this.image}'>
            </div>
            <div class='col'>
              <a href="${this.link}">
                <h4>${this.name}</h4>
              </a>
              <p>${this.description}</p>
              <div class='row'>
                
                <button class='btn like' data-id="${this.id}">Like ${this.likes}</button>
              </div>
            </div>
        `
      target.innerHTML = innerHTML;
      let likebtn = target.querySelector('.like');
      likebtn.addEventListener('click', this.buyItem);
    }
    
  }