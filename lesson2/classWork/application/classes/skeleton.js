import { Post, EdvartPost } from './post';
import data from './data';

function storFromClass() {
  console.log(this);
  this.store();
}

class Skeleton {

    constructor() {
      this.lsName = 'scel';
      this.dates = [];
      let restdata = this.getFromStor();
      if (restdata.length < 1) { restdata = data; }
      restdata.map(x => {
        if (this.count % 3) {
          let post = new Post(x);
          post.store = storFromClass.bind(this);
          this.dates.push(post);
        } else {
          let post = new EdvartPost(x);
          this.dates.push(post);
        }
      });
      this.store();
    }

    render() {
      const renderArea = document.getElementById('renderArea');
      let div = document.createElement('div');
      div.className = 'row';
      this.dates.map(x => {
        let div = document.createElement('div');
        div.className = 'post';
        x.render(div);
        renderArea.appendChild(div);
      })
    }

    store() {
      localStorage.setItem(this.lsName, JSON.stringify(this.dates));
    }

    getFromStor() {
      return JSON.parse(localStorage.getItem(this.lsName));
    }

    // restore() {
    //   const restdata = this.getFromStor();
    //   restdata.map((item, index) => {
    //     for (let field in item) {
    //       if (item[field] !== this.dates[index][field]) {
    //         this.dates[index][field] = item[field];
    //       }
    //     }
    //   })
    // }
  }