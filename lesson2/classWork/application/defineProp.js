
/*

    Задание, написать класс, который создает объекты по модельке:
    Dog {
        name: "", -> not configurable
        breed: "", -> not configurable, not editable
        weight: "",
        isGoodBoy: true -> enumerable 
    }   

*/

export class Dog {
    constructor(name, breed, weight, isGoodBoy = true) {
        Object.defineProperty( this, 'prop', { 
            value: "class Dog"
        });
        Object.defineProperty( this, 'name', { 
            writable: true,
            configurable: false,
            enumerable: true,
            value: name
        });
        Object.defineProperty( this, 'breed', { 
            enumerable: true,
            value: breed
        });
        this.weight = weight;
        Object.defineProperty( this, 'isGoodBoy', { 
            enumerable: true,
            value: isGoodBoy
        });
    }
    about() {
        return `I'm a ${this.name} and I'm ${this.breed}. I've ${this.weight} kg of goodness and it's ${this.isGoodBoy}`;
    };
}