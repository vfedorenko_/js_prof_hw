import './css/index.css'

// import Skeleton from "./classes";
import {Dog} from './defineProp'

// window.addEventListener('load', new Skeleton().render());

window.addEventListener('load', () => {
    let jack = new Dog("Jack", "Taxe", 1.5, true);
    console.log(jack.about());
    // delete jack.name;
    // jack.breed = "puddle";
    for (let key in jack) {
        console.log(jack[key]);
    }
});