

  /*

    Задание:

      1. Создать конструктор бургеров на прототипах, которая добавляет наш бургер в массив меню

      Дожно выглядеть так:
      new burger('Hamburger',[ ...Массив с ингредиентами ] , 20);

      Моделька для бургера:
      {
        cookingTime: 0,     // Время на готовку
        showComposition: function(){
          let {composition, name} = this;
          let compositionLength = composition.length;
          console.log(compositionLength);
          if( compositionLength !== 0){
            composition.map( function( item ){
                console.log( 'Состав бургера', name, item );
            })
          }
        }
      }

      Результатом конструктора нужно вывести массив меню c добавленными элементами.
      // menu: [ {name: "", composition: [], cookingTime:""},  {name: "", composition: [], cookingTime:""}]

        2. Создать конструктор заказов

        Моделька:
        {
          id: "",
          orderNumber: "",
          orderBurder: "",
          orderException: "",
          orderAvailability: ""
        }

          Заказ может быть 3х типов:
            1. В котором указано название бургера
              new Order('Hamburger'); -> Order 1. Бургер Hamburger, будет готов через 10 минут.
            2. В котором указано что должно быть в бургере, ищете в массиве Menu подходящий вариант
              new Order('', 'has', 'Название ингредиента') -> Order 2. Бургер Чизбургер, с сыром, будет готов через 5 минут.
            3. В котором указано чего не должно быть
              new Order('', 'except', 'Название ингредиента') ->


            Каждый их которых должен вернуть статус:
            "Заказ 1: Бургер ${Название}, будет готов через ${Время}

            Если бургера по условиям заказа не найдено предлагать случайный бургер из меню:
              new Order('', 'except', 'Булка') -> К сожалению, такого бургера у нас нет, можете попробовать "Чизбургер"
              Можно например спрашивать через Confirm подходит ли такой вариант, если да - оформлять заказ
              Если нет, предложить еще вариант из меню.

        3. Протестировать программу.
          1. Вначале добавляем наши бургеры в меню (3-4 шт);
          2. Проверяем работу прототипного наследования вызывая метод showComposition на обьект бургера с меню
          3. Формируем 3 заказа

        + Бонусные задания:
        4. Добавлять в исключения\пожелания можно несколько ингредиентов

  */

  const Ingredients = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Бекон',
    'Рыбная котлета',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Виолла',
    'Сыр Гауда',
    'Майонез'
  ];

  let OurMenu = [];
  const showMenu = () => {
    OurMenu.map(x => x.showComposition());
  }
  
  
  let OurOrders = [];
  OurOrders.count = 0;

  OurOrders.pushBurger = function(orderedBurger) {
    orderedBurger.orderNumber = ++(this.count);
    OurOrders.push(orderedBurger);
    console.log(`Заказ ${orderedBurger.orderNumber}: Бургер ${orderedBurger.orderedBurger}, будет готов через ${orderedBurger.time}`);
  }
  

  console.log(OurMenu, OurOrders);

  // class new Burger('Hamburger',[ ...Массив с ингредиентами ] , 20);
  function Burger( name, ingredients, cookingTime){
    this.name = name;
    this.ingredients = ingredients;
    // this.ingredients = [];
    // let ingr = ingredients;
    // ingr.map(item => {this.ingredients.push(checkIfValue(item))});
    if (typeof((+cookingTime) === 'number')) {
      this.cookingTime = cookingTime;
    } else {
      throw 'Wrong time value! Should be a number.'
    }
    OurMenu.push(this);
    showMenu();
  };
  Burger.prototype.showComposition = function () {
    let {ingredients, name} = this;
    let compositionLength = ingredients.length;
    console.log(compositionLength);
    if( compositionLength !== 0){
      ingredients.map( function( item ){
          console.log( 'Состав бургера: ', name, item );
      })
    }
  };

  // class Order: new Order('Hamburger');
  //              new Order('', 'has', 'Название ингредиента');
  //              new Order('', 'except', 'Название ингредиента');
  let count = 0;
  function Order(name = '', condition = '', value = []){
    try {
      this.orderedBurger = '';
      this.orderAvailability = [];
      this.orderException = [];
      
      this.time = 0;
      if (!(this.checkIfName(name))) {              // if name
        this.checkIfCondition(condition, value);    // if condition with values
        let one = findOne(this); 
        if (one) {
          this.orderedBurger = one[0];
          this.time = one[1];
        }                           // looking for our sendwich if it exist in menu
      }
      if (!(this.orderedBurger)) {
        const one = OurMenu[0].name;
        let burger = prompt(`К сожалению, такого бургера у нас нет, можете попробовать "${one}"`) ? one : undefined;
        console.log(burger);
        if (!burger) {
          console.warn("You ordered nothing!");
          return;
        }
        this.checkIfName(burger);
      }
      this.id = ++count;
      OurOrders.pushBurger(this);             // add to the order list
    }
    catch (e) {
      console.error(e);
      return;
    }
  }

  Order.prototype.checkIfName = function(name) {
    name = name.toLowerCase();
    if (name) {
      const result = OurMenu.filter(burger => burger.name.toLowerCase() === name)
      if (result.length > 0) {
        this.orderedBurger = result[0].name;
        this.time = result[0].cookingTime;
        return result;
      } 
      throw 'Invalid name! Look at menu!';
    }
    return false;
  };

  Order.prototype.checkIfCondition = function(condition, value) {
    value = Array.from(value);
    if (condition === 'has') {
      value.map((item) => {this.orderAvailability.push(checkIfValue(item))});
      this.orderAvailability = shiftUndef(this.orderAvailability);
    } else if (condition === 'except') {
      value.map(x => this.orderException.push(checkIfValue(x)));
      this.orderException = shiftUndef(this.orderException);
    } else {
      throw "Wrong condition value. Set 'has' or 'except'!";
    }
  };
  // class Order.

  const checkIfValue = (value) => {               // check if ingredient is in the order
    return Ingredients.find(function(item) { return item === value; });
  };

  const findOne = (obj) => {
    if (obj.orderAvailability.length > 0) {
      for (item of OurMenu) {
        if (obj.orderAvailability.every(val => item.ingredients.find(x => x === val))) {
          return [item.name, item.cookingTime];
        }
      }
      return  undefined;                          // return first entry or undefined
    } else if (obj.orderException.length > 0) {
      for (item of OurMenu) {
        if (!obj.orderException.every(function(val) { return !item.ingredients.find( x => x === val ) })) {
          console.log("Deny", item );
          continue;
        } else {
          console.log("acsept", item );
          return [item.name, item.cookingTime];
        }
      }
      return  undefined;                //return first entry or undefined
    }
    else {
      return undefined;                          // return undefined if nothing was founded
    }
  }

  const shiftUndef = (arr) => {                 //deletes all items has value undefined
    return arr.filter(function(item) { return item !== undefined; });
  }

  const IngredChees = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Помидорка',
    'Маслины',
    'Кунжут',
    'Сыр Чеддер',
    'Сыр Гауда',
    'Майонез'
  ];
  const cheesburger = new Burger('Cheesburger', IngredChees, 20);

  const IngredHamb = [
    'Булка',
    'Бекон',
    'Соус карри',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Сыр Чеддер',
  ];
  const hamburger = new Burger('Hamburger', IngredHamb, 20);

  const Hot = [
    'Булка',
    'Огурчик',
    'Котлетка',
    'Соус карри',
    'Помидорка',
    'Капуста',
    'Майонез'
  ];
  const hotdog = new Burger('Hot-dog', Hot, 5);

  const shaur = [
    'Булка',
    'Рыбная котлета',
    'Кисло-сладкий соус',
    'Помидорка',
    'Маслины',
    'Острый перец',
    'Капуста',
    'Кунжут',
    'Майонез'
  ];
  const shaurma = new Burger('Shaurma', shaur, 10);

  new Order('Hamburger');
  new Order('Hamburtger');
  
  const unlike = [  //hot-dog
    'Бекон',
    'Маслины',
    'Рыбная котлета',
    'Кисло-сладкий соус',
  ];
  new Order('', 'has', IngredHamb);
  new Order('', 'except', unlike);
  new Order('', 'except', Ingredients);
  