/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./src/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./src/app/task1.js":
/*!**************************!*\
  !*** ./src/app/task1.js ***!
  \**************************/
/*! exports provided: BackendDeveloper, FrontendDeveloper, Designer, ProjectManager */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"BackendDeveloper\", function() { return BackendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"FrontendDeveloper\", function() { return FrontendDeveloper; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"Designer\", function() { return Designer; });\n/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, \"ProjectManager\", function() { return ProjectManager; });\n/*\n  Composition:\n\n  Задание при помощи композиции создать объекты 4х типов:\n\n  functions:\n    - MakeBackendMagic\n    - MakeFrontendMagic\n    - MakeItLooksBeautiful\n    - DistributeTasks\n    - DrinkSomeTea\n    - WatchYoutube\n    - Procrastinate\n\n  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate\n  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube\n  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate\n  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea\n\n  ProjectManager(name,gender,age) => { name, gender, age, type: 'project'}\n\n*/\n\n  const MakeBackendMagic = (state) => ({\n    MakeBackendMagic: () => console.log(\"I make a Back-End Magic\")\n  });\n\n  const MakeFrontendMagic = (state) => ({\n    MakeFrontendMagic: () => console.log(\"I make a Front-End Magic\")\n  });\n\n  const MakeItLooksBeautiful = (state) => ({\n    MakeItLooksBeautiful: () => console.log(\"I make it looks like a Magic\")\n  });\n\n  const DrinkSomeTea = (state) => ({\n    DrinkSomeTea: () => console.log(\"I drink Coffee\")\n  });\n\n  const WatchYoutube = (state) => ({\n    WatchYoutube: () => console.log(\"You'll sea in youtube\")\n  });\n\n  const Procrastinate = (state) => ({\n    Procrastinate: () => console.log(\"I'm not sure what it meens\")\n  });\n\n  const BackendDeveloper = ( name, gender, age ) => {\n    const state = {\n      name,\n      gender,\n      age,\n      type: 'BackendDeveloper'\n    }\n    return Object.assign(\n      {},\n      state,\n      MakeBackendMagic(state),\n      DrinkSomeTea(state),\n      Procrastinate(state)\n    )\n  }\n\n  const FrontendDeveloper = ( name, gender, age ) => {\n    const state = {\n      name,\n      gender,\n      age,\n      type: 'FrontendDeveloper'\n    }\n    return Object.assign(\n      {},\n      state,\n      MakeFrontendMagic(state),\n      DrinkSomeTea(state),\n      WatchYoutube(state)\n    )\n  }\n\n  const Designer = ( name, gender, age ) => {\n    const state = {\n      name,\n      gender,\n      age,\n      type: 'Designer'\n    }\n    return Object.assign(\n      {},\n      state,\n      MakeItLooksBeautiful(state),\n      WatchYoutube(state),\n      Procrastinate(state)\n    )\n  }\n\n  const ProjectManager = ( name, gender, age ) => {\n    const state = {\n      name,\n      gender,\n      age,\n      type: 'ProjectManager'\n    }\n    return Object.assign(\n      {},\n      state,\n      DistributeTasks(state),\n      DrinkSomeTea(state),\n      Procrastinate(state)\n    )\n  }\n\n\n\n\n//# sourceURL=webpack:///./src/app/task1.js?");

/***/ }),

/***/ "./src/index.js":
/*!**********************!*\
  !*** ./src/index.js ***!
  \**********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
eval("__webpack_require__.r(__webpack_exports__);\n/* harmony import */ var _app_task1__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./app/task1 */ \"./src/app/task1.js\");\n// import { universe, deepFreeze} from './app';\n// import Government from './app/singleton'\n\n\nwindow.addEventListener('load', function() {\n    // let FarGalaxy = deepFreeze(universe);\n    //   // FarGalaxy.good.push('javascript'); // false\n    //   FarGalaxy.something = 'Wow!'; // false\n    //   FarGalaxy.evil.humans = [];   // false\n    //   // universe.good.push('javascript'); // false\n    //   universe.something = 'Wow!'; // false\n    //   universe.evil.humans = [];   // false\n    //   console.log(FarGalaxy);\n    //   console.log(universe);\n      \n\n    // Government.addLaw('name', 'text');\n    // Government.addLaw('name2', 'texttexttexttext');\n    // Government.readConstitution();\n    // Government.readLaw(1);\n    // Government.showSatisfy();\n    // Government.showBudget();\n    // Government.fest();\n    // Government.showSatisfy();\n    // Government.showBudget();\n\n\n    const frontdev = Object(_app_task1__WEBPACK_IMPORTED_MODULE_0__[\"FrontendDeveloper\"])('viktor', 'male', 32);\n    this.console.log(frontdev);\n\n\n})\n\n//# sourceURL=webpack:///./src/index.js?");

/***/ })

/******/ });