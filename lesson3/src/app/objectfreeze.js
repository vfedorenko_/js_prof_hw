/*
  Задание: написать функцию, для глубокой заморозки обьектов.

  Обьект для работы:
  

  frozenUniverse.evil.humans = []; -> Не должен отработать.

  Методы для работы:
  1. Object.getOwnPropertyNames(obj);
      -> Получаем имена свойств из объекта obj в виде массива

  2. Проверка через typeof на обьект или !== null
  if (typeof prop == 'object' && prop !== null){...}

  Тестирование:

  let FarGalaxy = DeepFreeze(universe);
      FarGalaxy.good.push('javascript'); // false
      FarGalaxy.something = 'Wow!'; // false
      FarGalaxy.evil.humans = [];   // false

*/
export let universe = {
  infinity: Infinity,
  good: ['cats', 'love', 'rock-n-roll'],
  evil: {
    bonuses: ['cookies', 'great look']
  }
};

export const deepFreeze = (obj) => {
  let keys = Object.getOwnPropertyNames(obj);
  console.log(keys)
  keys.forEach(item => {
    if (typeof obj[item] == 'object' && obj[item] !== null){
      obj[item] = deepFreeze(obj[item]);
    } 
  })
  return Object.freeze(obj)
}

// export default { universe, FarGalaxy };
