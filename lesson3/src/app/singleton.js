/*
  Задание:

    Написать синглтон, который будет создавать обьект government

    Данные:
    {
        laws: [
        {
          id: 0,
          text: '123123'
        }
      ],
        budget: 1000000
        citizensSatisfactions: 0,
    }

    У этого обьекта будут методы:
      .добавитьЗакон({id, name, description})
        -> добавляет закон в laws и понижает удовлетворенность граждан на -10

      .читатькКонституцию -> Вывести все законы на экран
      .читатьЗакон(ид)

      .показатьУровеньДовольства()
      .показатьБюджет()
      .провестиПраздник -> отнимает от бюджета 50000, повышает удовлетворенность граждан на +5


*/
class Law {
  static count = 0;

  constructor( name, text ) {
    this.id = Law.count++;
    this.name = name;
    this.text = text;
  };
  
};


const _government = {
  laws: [],
  budget: 1000000,
  citizensSatisfactions: 0,
};

const Government = {
  addLaw: ( name, text) => {  let law = new Law( name, text );
                              _government.laws.push(law);
                              _government.citizensSatisfactions -= 10 },
  readConstitution: () => _government.laws.map(item => console.log(`
                          LAW N^ ${item.id}, "${item.name}"\n${item.text}`)),
  readLaw: (id) => { let item = _government.laws.find(d => d.id === id);
                     console.log(`LAW N^ ${item.id}, "${item.name}"\n${item.text}`) },
  showSatisfy: () => console.log('Citizens are satisfyed by ' + _government.citizensSatisfactions),
  showBudget: () => console.log('The Budget is ' + _government.budget + "wright now! It's bed..."),
  fest: () => { _government.budget -= 50000;
                _government.citizensSatisfactions += 5; 
                console.log('We celebrate now! Have a fun.'); }
}

Object.freeze(Government);

export default Government;