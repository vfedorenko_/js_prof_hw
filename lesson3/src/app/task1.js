/*
  Composition:

  Задание при помощи композиции создать объекты 4х типов:

  functions:
    - MakeBackendMagic
    - MakeFrontendMagic
    - MakeItLooksBeautiful
    - DistributeTasks
    - DrinkSomeTea
    - WatchYoutube
    - Procrastinate

  BackendDeveloper = MakeBackendMagic + DrinkSomeTea + Procrastinate
  FrontendDeveloper = MakeFrontendMagic + DrinkSomeTea + WatchYoutube
  Designer = MakeItLooksBeautiful + WatchYoutube + Procrastinate
  ProjectManager = DistributeTasks + Procrastinate + DrinkSomeTea

  ProjectManager(name,gender,age) => { name, gender, age, type: 'project'}

*/

  const MakeBackendMagic = (state) => ({
    MakeBackendMagic: () => console.log("I make a Back-End Magic")
  });

  const MakeFrontendMagic = (state) => ({
    MakeFrontendMagic: () => console.log("I make a Front-End Magic")
  });

  const MakeItLooksBeautiful = (state) => ({
    MakeItLooksBeautiful: () => console.log("I make it looks like a Magic")
  });

  const DrinkSomeTea = (state) => ({
    DrinkSomeTea: () => console.log("I drink Coffee")
  });

  const WatchYoutube = (state) => ({
    WatchYoutube: () => console.log("You'll sea in youtube")
  });

  const Procrastinate = (state) => ({
    Procrastinate: () => console.log("I'm not sure what it meens")
  });

  export const BackendDeveloper = ( name, gender, age ) => {
    const state = {
      name,
      gender,
      age,
      type: 'BackendDeveloper'
    }
    return Object.assign(
      {},
      state,
      MakeBackendMagic(state),
      DrinkSomeTea(state),
      Procrastinate(state)
    )
  }

  export const FrontendDeveloper = ( name, gender, age ) => {
    const state = {
      name,
      gender,
      age,
      type: 'FrontendDeveloper'
    }
    return Object.assign(
      {},
      state,
      MakeFrontendMagic(state),
      DrinkSomeTea(state),
      WatchYoutube(state)
    )
  }

  export const Designer = ( name, gender, age ) => {
    const state = {
      name,
      gender,
      age,
      type: 'Designer'
    }
    return Object.assign(
      {},
      state,
      MakeItLooksBeautiful(state),
      WatchYoutube(state),
      Procrastinate(state)
    )
  }

  export  const ProjectManager = ( name, gender, age ) => {
    const state = {
      name,
      gender,
      age,
      type: 'ProjectManager'
    }
    return Object.assign(
      {},
      state,
      DistributeTasks(state),
      DrinkSomeTea(state),
      Procrastinate(state)
    )
  }


