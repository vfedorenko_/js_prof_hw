const MakeBackendMagic = (state) => ({
    MakeBackendMagic: () => console.log("I make a Back-End Magic")
});

const MakeFrontendMagic = (state) => ({
    MakeFrontendMagic: () => console.log("I make a Front-End Magic")
});

const MakeItLooksBeautiful = (state) => ({
    MakeItLooksBeautiful: () => console.log("I make it looks like a Magic")
});

const DrinkSomeTea = (state) => ({
    DrinkSomeTea: () => console.log("I drink Coffee")
});

const WatchYoutube = (state) => ({
    WatchYoutube: () => console.log("You'll sea in youtube")
});

const Procrastinate = (state) => ({
    Procrastinate: () => console.log("I'm not sure what it meens")
});

export const BackendDeveloper = ( name ) => {
    const state = Object.assign( name );
    return Object.assign(
        {},
        state,
        MakeBackendMagic(state),
        DrinkSomeTea(state),
        Procrastinate(state)
    )
}

export const FrontendDeveloper = ( name ) => {
    const state = Object.assign( name );
    return Object.assign(
        {},
        state,
        MakeFrontendMagic(state),
        DrinkSomeTea(state),
        WatchYoutube(state)
    )
}

export const Designer = ( name ) => {
    const state = Object.assign( name );
    return Object.assign(
        {},
        state,
        MakeItLooksBeautiful(state),
        WatchYoutube(state),
        Procrastinate(state)
    )
}

export  const ProjectManager = ( name ) => {
    const state = Object.assign( name );
    return Object.assign(
        {},
        state,
        DistributeTasks(state),
        DrinkSomeTea(state),
        Procrastinate(state)
    )
}
