import './composition';

class HireFactory {
    makeHire( person ){
        let Hierer = null;
        switch (person.type) {
            case 'backend': 
                Hierer = BackendDeveloper(person);
                break;

            case 'frontend': 
                Hierer = FrontendDeveloper(person);
                break;

            case 'project': 
                Hierer = ProjectManager(person);
                break;

            case 'design': 
                Hierer = Designer(person);
                break;

            default:
                return false;
        }
                	
        return Hierer;
    }
}


export const Fabric = (person, team = null) => {

    const mySuperForge = new HireFactory();
    let hirer = mySuperForge.makeHire(person);

    if (team) {
        if (hirer) {
            team[person.type].push(hirer);
        } else {
            team['othet'].push(hierer);
        }
    }

    return hirer;
    
}