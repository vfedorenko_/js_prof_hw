import Factory from 'factory.js';

const uri = 'http://www.json-generator.com/api/json/get/cfeTmcNIXS?indent=2';
let staff = {
	"backend": [],
	'frontend': [],
	'project': [],
	'design': []
}

const hireHirer = (e) => {
	e.preventDefault();
	let face = new Fabric(this);
	staff[face.type].map(item => {
		if (item._id === face._id) {
			staff[face.type].pop(item);
			e.target.innerHTML = "Hire"
			return 0;
		}
	})
	staff[this.type].push(face);
	e.target.innerHTML = "Hired"
};

const render = (obj) => {
	const parent = document.createElement('div');
	let rend = `<ul data-id="${obj[_id]}"> `;
	for (field in obj) {
		rend += `<li data-name="${field}">${obj[field]}</li>`
	};
	let hireOne = hireHirer.bind(obj);
	rend += `<button type="submit" onclick="${hireOne}">Hire</button></ul>`;
	parent.innerHTML = rend;
	return parent;
}





window.addEventListener('load', function() {
	fetch(uri)
		.then(response => response.json())
		.then(data => {
			console.log(data);
			data.forEach(item => {
				document.body.appendChild(render(item));
			});
		})	
})