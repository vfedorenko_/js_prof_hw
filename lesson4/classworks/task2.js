/*
    Задание:  Открыть файл task1.html в папке паблик и настроить светофоры в
                соответсвии с правилавми ниже:

    1. Написать кастомные события которые будут менять статус светофора:
    - start: включает зеленый свет
    - stop: включает красный свет
    - night: включает желтый свет, который моргает с интервалом в 1с.
    И зарегистрировать каждое через addEventListener на каждом из светофоров.

    2.  Сразу после загрузки на каждом светофоре вызывать событие night, для того,
        чтобы включить режим "нерегулируемого перекрестка" (моргающий желтый).

    3.  По клику на любой из светофоров нунжо включать на нем поочередно красный (не первый клик)
        или зеленый (на второй клик) цвет соотвественно.
        Действие нужно выполнить только диспатча событие зарегистрированое в пункте 1.

    4.  + Бонус: На кнопку "Start Night" повесить сброс всех светофоров с их текущего
        статуса, на мигающий желтые.
        Двойной, тройной и более клики на кнопку не должны вызывать повторную
        инициализацию инвервала.
*/

let lighter_start = new CustomEvent("start", {
	detail: {
        target: "lighter",
        color: "green"
	}
});

let lighter_stop = new CustomEvent("stop", {
	detail: {
        target: "lighter",
        color: "red"
	}
});

let lighter_night = new CustomEvent("night", {
	detail: {
        target: "lighter",
        color: "yellow"
	}
});

const start_func = (e) => {
    night_array = night_array.filter(function(item) { return item.id !== e.target.id; });
    e.target.classList.add("green");
    e.target.classList.remove("red");
    e.target.classList.remove("yellow"); 
    console.log(night_array)  
}

const stop_func = (e) => {
    night_array = night_array.filter(function(item) { return item.id !== e.target.id; });
    e.target.classList.add("red");
    e.target.classList.remove("green");
    e.target.classList.remove("yellow");
    console.log(night_array) 
}

let timer = 1;
const night_func = (e) => {

    e.target.classList.remove("green");
    e.target.classList.remove("red");
    e.target.classList.remove("yellow");
    // while ( timer ) {
        if (timer % 2 === 0) {
            e.target.classList.add("yellow");
        } else {
            e.target.classList.remove("yellow");
        }
    // }
    
};

let night_array = [];

window.addEventListener('load', function(e) {
    
    const night = document.getElementById("Do");
    const tlighters = document.querySelectorAll(".trafficLight");

    const click = new Event('click');
    
    tlighters.forEach(item => {
        item.dataset.stat = 0;
        item.addEventListener("click", function(e) {
            let { stat } = e.target.dataset
            console.log(stat, +stat)
            if (+stat === 1) {
                e.target.dispatchEvent(lighter_start);
                e.target.dataset.stat = 0;
            } else {
                e.target.dispatchEvent(lighter_stop);
                e.target.dataset.stat = 1;
            }
        });
        item.addEventListener("start", start_func);
        item.addEventListener("stop", stop_func);
        item.addEventListener("night", night_func);
    });

    
    night.addEventListener('click', function(e) {
        e.preventDefault();
        night_array = [];
        tlighters.forEach(item => {
            item.dataset.stat = 0;
            night_array.push(item);
        })
    });

    night.dispatchEvent( click );

    let interval = setInterval(function() { timer++; 
                                            console.log(timer);
                                            console.log(night_array) 
                                            if (night_array.length > 0) {
                                                night_array.map(item => item.dispatchEvent(lighter_night));
                                            };
                                          }, 1000);

});