/*

  Задание: используя паттерн декоратор, модифицировать класс Human из примера basicUsage.

  0.  Создать новый конструктор, который будет принимать в себя человека как аргумент,
      и будем добавлять ему массив обьектов coolers (охладители), а него внести обьекты
      например мороженное, вода, сок и т.д в виде: {name: 'icecream', temperatureCoolRate: -5}

  1.  Расширить обработку функции ChangeTemperature в прототипе human таким образом,
      что если темпаретура становится выше 30 градусов то мы берем обьект из массива coolers
      и "охлаждаем" человека на ту температуру которая там указана.

      Обработку старого события если температура уходит вниз поставить с условием, что температура ниже нуля.
      Если температура превышает 50 градусов, выводить сообщение error -> "{Human.name} зажарился на солнце :("

  2.  Бонус: добавить в наш прототип нашего нового класса метод .addCooler(), который
      будет добавлять "охладитель" в наш обьект. Сделать валидацию что бы через этот метод
      нельзя было прокинуть обьект в котором отсутствует поля name и temperatureCoolRate.
      Выводить сообщение с ошибкой про это.

*/

import { Human } from './human';

const BeachParty = () => {

    console.log( 'your code ');

    function CooledHuman( Human ){
        this.name = Human.name;
        this.coolers = [
            { name: 'icecream', temperatureResistance: 5},
            { name: 'water', temperatureResistance: 3},
            { name: 'jus', temperatureResistance: 3},
        ];
        this.currentTemperature = 30;
        this.maxTemperature = Human.maxTemperature - this.coolers.reduce(
            ( currentResistance, cooler ) => {
                console.log('currentResistance', currentResistance,  'cooler', cooler);
                return currentResistance - cooler.temperatureResistance;
            }, 0
        );
        this.minTemperature = Human.minTemperature;
        console.log(`new Human ${this.name} arrived! He can survive in temperature ${this.maxTemperature}`, this);
      }
      CooledHuman.prototype = Object.create( Human.prototype );
  
      let Dexter = new CooledHuman( new Human('Dexter') );
      console.log( Dexter );
          Dexter.ChangeTemperature(5);
          Dexter.ChangeTemperature(10);
          Dexter.ChangeTemperature(15);
          Dexter.ChangeTemperature(-40);

  
    }
  
  export default BeachParty;
  