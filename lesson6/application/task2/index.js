import { Notifier } from './notifier';

export class Application {
    constructor( notifiers ) {
      this.noteTargets = notifiers;
      this.notifiers = new Notifier( this.noteTargets );
      this.render = this.render.bind(this);
    }

    render() {
      const root = document.getElementById('root');
      const AppNode = document.createElement('section');
  
      AppNode.className = 'notifer_app';
      AppNode.innerHTML =
      `
        <div class="notifer_app--container">
          <header>
            <input class="notifier__messanger" type="text"/>
            <button class="notifier__send">Send Message</button>
          </header>
          <div class="notifier__container">
          ${
            this.noteTargets.map( item =>
              `
              <div class="notifier__item" data-slug="${item.type}--${item.contact}">
                <header class="notifier__header">
                  <img width="25" src="public/images/${item.type}.svg"/>
                  <span>${item.contact}</span>
                </header>
                <ul class="notifier__messages"></ul>
              </div>
              `).join('')
          }
          </div>
        </div>
      `;
      const container = AppNode.querySelector('.notifier__container');
      const btn = AppNode.querySelector('.notifier__send');
      const input = AppNode.querySelector('.notifier__messanger');
      btn.addEventListener('click', () => {
        let value = input.value;
        this.notifiers.sendNews( value, container );
      });
  
      this.node = AppNode;
      root.appendChild(AppNode);
    }
}



















