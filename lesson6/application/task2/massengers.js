export class Massenger {
    constructor( { contact } ) {
        this.contact = contact;
    }
    send(baseNode, msg, contact) {
        const parent = baseNode.querySelector(`.notifier__item[data-slug="${contact}"]`); 
        const target = parent.querySelector('.notifier__messages');
        target.innerHTML += `<li>${msg}</li>`;
    }
}

export class Telegram extends Massenger {
    send(baseNode, msg) {
        let cont = `telegram--${this.contact}`
        super.send(baseNode, msg, cont);
    }
}

export class Viber extends Massenger {
    send(baseNode, msg) {
        let cont = `viber--${this.contact}`
        super.send(baseNode, msg, cont);
    }
}

export class Sms extends Massenger {
    send(baseNode, msg) {
        let cont = `sms--${this.contact}`
        super.send(baseNode, msg, cont);
    }
}

export class Gmail extends Massenger {
    send(baseNode, msg) {
        let cont = `gmail--${this.contact}`
        super.send(baseNode, msg, cont);
    }
}