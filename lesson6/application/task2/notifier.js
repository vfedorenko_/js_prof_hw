import { Telegram, Viber, Sms, Gmail } from './massengers';

export class Notifier {
    constructor( subscribers ) {
        let clients = subscribers.map ( one => {
            if (one.type === 'telegram') return new Telegram(one);
            else if (one.type === 'viber') return new Viber(one);
            else if (one.type === 'sms') return new Sms(one);
            else if (one.type === 'gmail') return new Gmail(one);
            else return null;
        } );
        this.clients = clients;
    };
    sendNews(msg, noda) {
        this.clients.map(one => { one.send( noda, msg ) } );
    };
}