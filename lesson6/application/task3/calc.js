const Work1 = () => {

    function fluent( fn ) {
        return function(...args) {
            fn.apply(this, args);
            console.log(args, this);
            return this;
        }
    }

    function fluentDecorator({ target, key, descriptor} ){
        try {
          console.log('target', target, 'key', key, 'descriptor', descriptor)
          const originFn = descriptor.value;
          descriptor.value = fluent(function( ...args ){
            let newargs = args.map(arg => {
                if ( typeof(arg) !== "number" ) {
                    arg = parseInt(arg);
                    if ( arg === NaN ) {
                        throw "Not a number"
                    }
                }
                return arg;
            });
            originFn.apply(this, newargs);
            return this;
          });
        } catch (e) {
            console.error( "Wrong Value", e );
        }
    }

    class CoolMath {
        @fluentDecorator
        addNumbers(a,b){ return a+b; };
        @fluentDecorator
        multiplyNumbers(a,b){ return a*b};
        @fluentDecorator
        minusNumbers(a,b){ return a-b };
    }
    let Calcul = new CoolMath();
    let x = Calcul.addNumbers(2, 2)
    let y = Calcul.multiplyNumbers("10", "2")
    let z = Calcul.minusNumbers(10, 2)
  
  };
  
  export default Work1;