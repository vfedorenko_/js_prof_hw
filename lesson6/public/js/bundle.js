/******/ (function(modules) { // webpackBootstrap
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "";
/******/
/******/
/******/ 	// Load entry module and return exports
/******/ 	return __webpack_require__(__webpack_require__.s = "./application/index.js");
/******/ })
/************************************************************************/
/******/ ({

/***/ "./application/index.js":
/*!******************************!*\
  !*** ./application/index.js ***!
  \******************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _task2__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./task2 */ "./application/task2/index.js");
// import BeachParty from './task1/cooledhuman';
 // BeachParty();

const Demo = () => {
  const NotifierApp = new _task2__WEBPACK_IMPORTED_MODULE_0__["Application"]([{
    contact: '09876544',
    type: 'sms',
    image: 'sms.svg'
  }, {
    contact: '09879874',
    type: 'sms',
    image: 'sms.svg'
  }, {
    contact: 'fgv@gbf.com',
    type: 'gmail',
    image: 'gmail.svg'
  }, {
    contact: '09876544',
    type: 'telegram',
    image: 'telegram.svg'
  }, {
    contact: '09876544',
    type: 'viber',
    image: 'viber.svg'
  }]);
  NotifierApp.render();
  console.log(NotifierApp); // const NotApp = new Application(
  //   [
  //     {name: 'mail', image: 'images/gmail.svg'},
  //     {name: 'telegram', image: 'images/telegram.svg'},
  //     {name: 'slack', image: 'images/slack.svg'},
  //   ]
  // );
  // NotApp.createInterface();
};

Demo();

/***/ }),

/***/ "./application/task2/index.js":
/*!************************************!*\
  !*** ./application/task2/index.js ***!
  \************************************/
/*! exports provided: Application */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Application", function() { return Application; });
/* harmony import */ var _notifier__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./notifier */ "./application/task2/notifier.js");

class Application {
  constructor(notifiers) {
    this.noteTargets = notifiers;
    this.notifiers = new _notifier__WEBPACK_IMPORTED_MODULE_0__["Notifier"](this.noteTargets);
    this.render = this.render.bind(this);
  }

  render() {
    const root = document.getElementById('root');
    const AppNode = document.createElement('section');
    AppNode.className = 'notifer_app';
    AppNode.innerHTML = `
        <div class="notifer_app--container">
          <header>
            <input class="notifier__messanger" type="text"/>
            <button class="notifier__send">Send Message</button>
          </header>
          <div class="notifier__container">
          ${this.noteTargets.map(item => `
              <div class="notifier__item" data-slug="${item.type}--${item.contact}">
                <header class="notifier__header">
                  <img width="25" src="public/images/${item.type}.svg"/>
                  <span>${item.contact}</span>
                </header>
                <ul class="notifier__messages"></ul>
              </div>
              `).join('')}
          </div>
        </div>
      `;
    const container = AppNode.querySelector('.notifier__container');
    const btn = AppNode.querySelector('.notifier__send');
    const input = AppNode.querySelector('.notifier__messanger');
    btn.addEventListener('click', () => {
      let value = input.value;
      this.notifiers.sendNews(value, container);
    });
    this.node = AppNode;
    root.appendChild(AppNode);
  }

}

/***/ }),

/***/ "./application/task2/massengers.js":
/*!*****************************************!*\
  !*** ./application/task2/massengers.js ***!
  \*****************************************/
/*! exports provided: Massenger, Telegram, Viber, Sms, Gmail */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Massenger", function() { return Massenger; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Telegram", function() { return Telegram; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Viber", function() { return Viber; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Sms", function() { return Sms; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Gmail", function() { return Gmail; });
class Massenger {
  constructor({
    contact
  }) {
    this.contact = contact;
  }

  send(baseNode, msg, contact) {
    const parent = baseNode.querySelector(`.notifier__item[data-slug="${contact}"]`);
    const target = parent.querySelector('.notifier__messages');
    target.innerHTML += `<li>${msg}</li>`;
  }

}
class Telegram extends Massenger {
  send(baseNode, msg) {
    let cont = `telegram--${this.contact}`;
    super.send(baseNode, msg, cont);
  }

}
class Viber extends Massenger {
  send(baseNode, msg) {
    let cont = `viber--${this.contact}`;
    super.send(baseNode, msg, cont);
  }

}
class Sms extends Massenger {
  send(baseNode, msg) {
    let cont = `sms--${this.contact}`;
    super.send(baseNode, msg, cont);
  }

}
class Gmail extends Massenger {
  send(baseNode, msg) {
    let cont = `gmail--${this.contact}`;
    super.send(baseNode, msg, cont);
  }

}

/***/ }),

/***/ "./application/task2/notifier.js":
/*!***************************************!*\
  !*** ./application/task2/notifier.js ***!
  \***************************************/
/*! exports provided: Notifier */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "Notifier", function() { return Notifier; });
/* harmony import */ var _massengers__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! ./massengers */ "./application/task2/massengers.js");

class Notifier {
  constructor(subscribers) {
    let clients = subscribers.map(one => {
      if (one.type === 'telegram') return new _massengers__WEBPACK_IMPORTED_MODULE_0__["Telegram"](one);else if (one.type === 'viber') return new _massengers__WEBPACK_IMPORTED_MODULE_0__["Viber"](one);else if (one.type === 'sms') return new _massengers__WEBPACK_IMPORTED_MODULE_0__["Sms"](one);else if (one.type === 'gmail') return new _massengers__WEBPACK_IMPORTED_MODULE_0__["Gmail"](one);else return null;
    });
    this.clients = clients;
  }

  sendNews(msg, noda) {
    this.clients.map(one => {
      one.send(noda, msg);
    });
  }

}

/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2s6Ly8vd2VicGFjay9ib290c3RyYXAiLCJ3ZWJwYWNrOi8vLy4vYXBwbGljYXRpb24vaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vYXBwbGljYXRpb24vdGFzazIvaW5kZXguanMiLCJ3ZWJwYWNrOi8vLy4vYXBwbGljYXRpb24vdGFzazIvbWFzc2VuZ2Vycy5qcyIsIndlYnBhY2s6Ly8vLi9hcHBsaWNhdGlvbi90YXNrMi9ub3RpZmllci5qcyJdLCJuYW1lcyI6WyJEZW1vIiwiTm90aWZpZXJBcHAiLCJBcHBsaWNhdGlvbiIsImNvbnRhY3QiLCJ0eXBlIiwiaW1hZ2UiLCJyZW5kZXIiLCJjb25zb2xlIiwibG9nIiwiY29uc3RydWN0b3IiLCJub3RpZmllcnMiLCJub3RlVGFyZ2V0cyIsIk5vdGlmaWVyIiwiYmluZCIsInJvb3QiLCJkb2N1bWVudCIsImdldEVsZW1lbnRCeUlkIiwiQXBwTm9kZSIsImNyZWF0ZUVsZW1lbnQiLCJjbGFzc05hbWUiLCJpbm5lckhUTUwiLCJtYXAiLCJpdGVtIiwiam9pbiIsImNvbnRhaW5lciIsInF1ZXJ5U2VsZWN0b3IiLCJidG4iLCJpbnB1dCIsImFkZEV2ZW50TGlzdGVuZXIiLCJ2YWx1ZSIsInNlbmROZXdzIiwibm9kZSIsImFwcGVuZENoaWxkIiwiTWFzc2VuZ2VyIiwic2VuZCIsImJhc2VOb2RlIiwibXNnIiwicGFyZW50IiwidGFyZ2V0IiwiVGVsZWdyYW0iLCJjb250IiwiVmliZXIiLCJTbXMiLCJHbWFpbCIsInN1YnNjcmliZXJzIiwiY2xpZW50cyIsIm9uZSIsIm5vZGEiXSwibWFwcGluZ3MiOiI7UUFBQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7O1FBR0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDBDQUEwQyxnQ0FBZ0M7UUFDMUU7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSx3REFBd0Qsa0JBQWtCO1FBQzFFO1FBQ0EsaURBQWlELGNBQWM7UUFDL0Q7O1FBRUE7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBLHlDQUF5QyxpQ0FBaUM7UUFDMUUsZ0hBQWdILG1CQUFtQixFQUFFO1FBQ3JJO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsMkJBQTJCLDBCQUEwQixFQUFFO1FBQ3ZELGlDQUFpQyxlQUFlO1FBQ2hEO1FBQ0E7UUFDQTs7UUFFQTtRQUNBLHNEQUFzRCwrREFBK0Q7O1FBRXJIO1FBQ0E7OztRQUdBO1FBQ0E7Ozs7Ozs7Ozs7Ozs7QUNsRkE7QUFBQTtBQUFBO0NBR0E7O0FBRUEsTUFBTUEsSUFBSSxHQUFHLE1BQU07QUFDZixRQUFNQyxXQUFXLEdBQUcsSUFBSUMsa0RBQUosQ0FDbEIsQ0FDRTtBQUFFQyxXQUFPLEVBQUUsVUFBWDtBQUE2QkMsUUFBSSxFQUFFLEtBQW5DO0FBQWlEQyxTQUFLLEVBQUU7QUFBeEQsR0FERixFQUVFO0FBQUVGLFdBQU8sRUFBRSxVQUFYO0FBQTZCQyxRQUFJLEVBQUUsS0FBbkM7QUFBaURDLFNBQUssRUFBRTtBQUF4RCxHQUZGLEVBR0U7QUFBRUYsV0FBTyxFQUFFLGFBQVg7QUFBNkJDLFFBQUksRUFBRSxPQUFuQztBQUFpREMsU0FBSyxFQUFFO0FBQXhELEdBSEYsRUFJRTtBQUFFRixXQUFPLEVBQUUsVUFBWDtBQUE2QkMsUUFBSSxFQUFFLFVBQW5DO0FBQWlEQyxTQUFLLEVBQUU7QUFBeEQsR0FKRixFQUtFO0FBQUVGLFdBQU8sRUFBRSxVQUFYO0FBQTZCQyxRQUFJLEVBQUUsT0FBbkM7QUFBaURDLFNBQUssRUFBRTtBQUF4RCxHQUxGLENBRGtCLENBQXBCO0FBU0FKLGFBQVcsQ0FBQ0ssTUFBWjtBQUVBQyxTQUFPLENBQUNDLEdBQVIsQ0FBYVAsV0FBYixFQVplLENBY2Y7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUVELENBdkJIOztBQXlCRUQsSUFBSSxHOzs7Ozs7Ozs7Ozs7QUM5Qk47QUFBQTtBQUFBO0FBQUE7QUFFTyxNQUFNRSxXQUFOLENBQWtCO0FBQ3JCTyxhQUFXLENBQUVDLFNBQUYsRUFBYztBQUN2QixTQUFLQyxXQUFMLEdBQW1CRCxTQUFuQjtBQUNBLFNBQUtBLFNBQUwsR0FBaUIsSUFBSUUsa0RBQUosQ0FBYyxLQUFLRCxXQUFuQixDQUFqQjtBQUNBLFNBQUtMLE1BQUwsR0FBYyxLQUFLQSxNQUFMLENBQVlPLElBQVosQ0FBaUIsSUFBakIsQ0FBZDtBQUNEOztBQUVEUCxRQUFNLEdBQUc7QUFDUCxVQUFNUSxJQUFJLEdBQUdDLFFBQVEsQ0FBQ0MsY0FBVCxDQUF3QixNQUF4QixDQUFiO0FBQ0EsVUFBTUMsT0FBTyxHQUFHRixRQUFRLENBQUNHLGFBQVQsQ0FBdUIsU0FBdkIsQ0FBaEI7QUFFQUQsV0FBTyxDQUFDRSxTQUFSLEdBQW9CLGFBQXBCO0FBQ0FGLFdBQU8sQ0FBQ0csU0FBUixHQUNDOzs7Ozs7O1lBUUssS0FBS1QsV0FBTCxDQUFpQlUsR0FBakIsQ0FBc0JDLElBQUksSUFDdkI7dURBQ3dDQSxJQUFJLENBQUNsQixJQUFLLEtBQUlrQixJQUFJLENBQUNuQixPQUFROzt1REFFM0JtQixJQUFJLENBQUNsQixJQUFLOzBCQUN2Q2tCLElBQUksQ0FBQ25CLE9BQVE7Ozs7ZUFMM0IsRUFTS29CLElBVEwsQ0FTVSxFQVRWLENBVUQ7OztPQW5CTDtBQXVCQSxVQUFNQyxTQUFTLEdBQUdQLE9BQU8sQ0FBQ1EsYUFBUixDQUFzQixzQkFBdEIsQ0FBbEI7QUFDQSxVQUFNQyxHQUFHLEdBQUdULE9BQU8sQ0FBQ1EsYUFBUixDQUFzQixpQkFBdEIsQ0FBWjtBQUNBLFVBQU1FLEtBQUssR0FBR1YsT0FBTyxDQUFDUSxhQUFSLENBQXNCLHNCQUF0QixDQUFkO0FBQ0FDLE9BQUcsQ0FBQ0UsZ0JBQUosQ0FBcUIsT0FBckIsRUFBOEIsTUFBTTtBQUNsQyxVQUFJQyxLQUFLLEdBQUdGLEtBQUssQ0FBQ0UsS0FBbEI7QUFDQSxXQUFLbkIsU0FBTCxDQUFlb0IsUUFBZixDQUF5QkQsS0FBekIsRUFBZ0NMLFNBQWhDO0FBQ0QsS0FIRDtBQUtBLFNBQUtPLElBQUwsR0FBWWQsT0FBWjtBQUNBSCxRQUFJLENBQUNrQixXQUFMLENBQWlCZixPQUFqQjtBQUNEOztBQTdDb0IsQzs7Ozs7Ozs7Ozs7O0FDRnpCO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQTtBQUFPLE1BQU1nQixTQUFOLENBQWdCO0FBQ25CeEIsYUFBVyxDQUFFO0FBQUVOO0FBQUYsR0FBRixFQUFnQjtBQUN2QixTQUFLQSxPQUFMLEdBQWVBLE9BQWY7QUFDSDs7QUFDRCtCLE1BQUksQ0FBQ0MsUUFBRCxFQUFXQyxHQUFYLEVBQWdCakMsT0FBaEIsRUFBeUI7QUFDekIsVUFBTWtDLE1BQU0sR0FBR0YsUUFBUSxDQUFDVixhQUFULENBQXdCLDhCQUE2QnRCLE9BQVEsSUFBN0QsQ0FBZjtBQUNBLFVBQU1tQyxNQUFNLEdBQUdELE1BQU0sQ0FBQ1osYUFBUCxDQUFxQixxQkFBckIsQ0FBZjtBQUNBYSxVQUFNLENBQUNsQixTQUFQLElBQXFCLE9BQU1nQixHQUFJLE9BQS9CO0FBQ0g7O0FBUmtCO0FBV2hCLE1BQU1HLFFBQU4sU0FBdUJOLFNBQXZCLENBQWlDO0FBQ3BDQyxNQUFJLENBQUNDLFFBQUQsRUFBV0MsR0FBWCxFQUFnQjtBQUNoQixRQUFJSSxJQUFJLEdBQUksYUFBWSxLQUFLckMsT0FBUSxFQUFyQztBQUNBLFVBQU0rQixJQUFOLENBQVdDLFFBQVgsRUFBcUJDLEdBQXJCLEVBQTBCSSxJQUExQjtBQUNIOztBQUptQztBQU9qQyxNQUFNQyxLQUFOLFNBQW9CUixTQUFwQixDQUE4QjtBQUNqQ0MsTUFBSSxDQUFDQyxRQUFELEVBQVdDLEdBQVgsRUFBZ0I7QUFDaEIsUUFBSUksSUFBSSxHQUFJLFVBQVMsS0FBS3JDLE9BQVEsRUFBbEM7QUFDQSxVQUFNK0IsSUFBTixDQUFXQyxRQUFYLEVBQXFCQyxHQUFyQixFQUEwQkksSUFBMUI7QUFDSDs7QUFKZ0M7QUFPOUIsTUFBTUUsR0FBTixTQUFrQlQsU0FBbEIsQ0FBNEI7QUFDL0JDLE1BQUksQ0FBQ0MsUUFBRCxFQUFXQyxHQUFYLEVBQWdCO0FBQ2hCLFFBQUlJLElBQUksR0FBSSxRQUFPLEtBQUtyQyxPQUFRLEVBQWhDO0FBQ0EsVUFBTStCLElBQU4sQ0FBV0MsUUFBWCxFQUFxQkMsR0FBckIsRUFBMEJJLElBQTFCO0FBQ0g7O0FBSjhCO0FBTzVCLE1BQU1HLEtBQU4sU0FBb0JWLFNBQXBCLENBQThCO0FBQ2pDQyxNQUFJLENBQUNDLFFBQUQsRUFBV0MsR0FBWCxFQUFnQjtBQUNoQixRQUFJSSxJQUFJLEdBQUksVUFBUyxLQUFLckMsT0FBUSxFQUFsQztBQUNBLFVBQU0rQixJQUFOLENBQVdDLFFBQVgsRUFBcUJDLEdBQXJCLEVBQTBCSSxJQUExQjtBQUNIOztBQUpnQyxDOzs7Ozs7Ozs7Ozs7QUNoQ3JDO0FBQUE7QUFBQTtBQUFBO0FBRU8sTUFBTTVCLFFBQU4sQ0FBZTtBQUNsQkgsYUFBVyxDQUFFbUMsV0FBRixFQUFnQjtBQUN2QixRQUFJQyxPQUFPLEdBQUdELFdBQVcsQ0FBQ3ZCLEdBQVosQ0FBa0J5QixHQUFHLElBQUk7QUFDbkMsVUFBSUEsR0FBRyxDQUFDMUMsSUFBSixLQUFhLFVBQWpCLEVBQTZCLE9BQU8sSUFBSW1DLG9EQUFKLENBQWFPLEdBQWIsQ0FBUCxDQUE3QixLQUNLLElBQUlBLEdBQUcsQ0FBQzFDLElBQUosS0FBYSxPQUFqQixFQUEwQixPQUFPLElBQUlxQyxpREFBSixDQUFVSyxHQUFWLENBQVAsQ0FBMUIsS0FDQSxJQUFJQSxHQUFHLENBQUMxQyxJQUFKLEtBQWEsS0FBakIsRUFBd0IsT0FBTyxJQUFJc0MsK0NBQUosQ0FBUUksR0FBUixDQUFQLENBQXhCLEtBQ0EsSUFBSUEsR0FBRyxDQUFDMUMsSUFBSixLQUFhLE9BQWpCLEVBQTBCLE9BQU8sSUFBSXVDLGlEQUFKLENBQVVHLEdBQVYsQ0FBUCxDQUExQixLQUNBLE9BQU8sSUFBUDtBQUNSLEtBTmEsQ0FBZDtBQU9BLFNBQUtELE9BQUwsR0FBZUEsT0FBZjtBQUNIOztBQUNEZixVQUFRLENBQUNNLEdBQUQsRUFBTVcsSUFBTixFQUFZO0FBQ2hCLFNBQUtGLE9BQUwsQ0FBYXhCLEdBQWIsQ0FBaUJ5QixHQUFHLElBQUk7QUFBRUEsU0FBRyxDQUFDWixJQUFKLENBQVVhLElBQVYsRUFBZ0JYLEdBQWhCO0FBQXVCLEtBQWpEO0FBQ0g7O0FBYmlCLEMiLCJmaWxlIjoiYnVuZGxlLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gVGhlIG1vZHVsZSBjYWNoZVxuIFx0dmFyIGluc3RhbGxlZE1vZHVsZXMgPSB7fTtcblxuIFx0Ly8gVGhlIHJlcXVpcmUgZnVuY3Rpb25cbiBcdGZ1bmN0aW9uIF9fd2VicGFja19yZXF1aXJlX18obW9kdWxlSWQpIHtcblxuIFx0XHQvLyBDaGVjayBpZiBtb2R1bGUgaXMgaW4gY2FjaGVcbiBcdFx0aWYoaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0pIHtcbiBcdFx0XHRyZXR1cm4gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0uZXhwb3J0cztcbiBcdFx0fVxuIFx0XHQvLyBDcmVhdGUgYSBuZXcgbW9kdWxlIChhbmQgcHV0IGl0IGludG8gdGhlIGNhY2hlKVxuIFx0XHR2YXIgbW9kdWxlID0gaW5zdGFsbGVkTW9kdWxlc1ttb2R1bGVJZF0gPSB7XG4gXHRcdFx0aTogbW9kdWxlSWQsXG4gXHRcdFx0bDogZmFsc2UsXG4gXHRcdFx0ZXhwb3J0czoge31cbiBcdFx0fTtcblxuIFx0XHQvLyBFeGVjdXRlIHRoZSBtb2R1bGUgZnVuY3Rpb25cbiBcdFx0bW9kdWxlc1ttb2R1bGVJZF0uY2FsbChtb2R1bGUuZXhwb3J0cywgbW9kdWxlLCBtb2R1bGUuZXhwb3J0cywgX193ZWJwYWNrX3JlcXVpcmVfXyk7XG5cbiBcdFx0Ly8gRmxhZyB0aGUgbW9kdWxlIGFzIGxvYWRlZFxuIFx0XHRtb2R1bGUubCA9IHRydWU7XG5cbiBcdFx0Ly8gUmV0dXJuIHRoZSBleHBvcnRzIG9mIHRoZSBtb2R1bGVcbiBcdFx0cmV0dXJuIG1vZHVsZS5leHBvcnRzO1xuIFx0fVxuXG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlcyBvYmplY3QgKF9fd2VicGFja19tb2R1bGVzX18pXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm0gPSBtb2R1bGVzO1xuXG4gXHQvLyBleHBvc2UgdGhlIG1vZHVsZSBjYWNoZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5jID0gaW5zdGFsbGVkTW9kdWxlcztcblxuIFx0Ly8gZGVmaW5lIGdldHRlciBmdW5jdGlvbiBmb3IgaGFybW9ueSBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmQgPSBmdW5jdGlvbihleHBvcnRzLCBuYW1lLCBnZXR0ZXIpIHtcbiBcdFx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhleHBvcnRzLCBuYW1lKSkge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBuYW1lLCB7IGVudW1lcmFibGU6IHRydWUsIGdldDogZ2V0dGVyIH0pO1xuIFx0XHR9XG4gXHR9O1xuXG4gXHQvLyBkZWZpbmUgX19lc01vZHVsZSBvbiBleHBvcnRzXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIgPSBmdW5jdGlvbihleHBvcnRzKSB7XG4gXHRcdGlmKHR5cGVvZiBTeW1ib2wgIT09ICd1bmRlZmluZWQnICYmIFN5bWJvbC50b1N0cmluZ1RhZykge1xuIFx0XHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCBTeW1ib2wudG9TdHJpbmdUYWcsIHsgdmFsdWU6ICdNb2R1bGUnIH0pO1xuIFx0XHR9XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShleHBvcnRzLCAnX19lc01vZHVsZScsIHsgdmFsdWU6IHRydWUgfSk7XG4gXHR9O1xuXG4gXHQvLyBjcmVhdGUgYSBmYWtlIG5hbWVzcGFjZSBvYmplY3RcbiBcdC8vIG1vZGUgJiAxOiB2YWx1ZSBpcyBhIG1vZHVsZSBpZCwgcmVxdWlyZSBpdFxuIFx0Ly8gbW9kZSAmIDI6IG1lcmdlIGFsbCBwcm9wZXJ0aWVzIG9mIHZhbHVlIGludG8gdGhlIG5zXG4gXHQvLyBtb2RlICYgNDogcmV0dXJuIHZhbHVlIHdoZW4gYWxyZWFkeSBucyBvYmplY3RcbiBcdC8vIG1vZGUgJiA4fDE6IGJlaGF2ZSBsaWtlIHJlcXVpcmVcbiBcdF9fd2VicGFja19yZXF1aXJlX18udCA9IGZ1bmN0aW9uKHZhbHVlLCBtb2RlKSB7XG4gXHRcdGlmKG1vZGUgJiAxKSB2YWx1ZSA9IF9fd2VicGFja19yZXF1aXJlX18odmFsdWUpO1xuIFx0XHRpZihtb2RlICYgOCkgcmV0dXJuIHZhbHVlO1xuIFx0XHRpZigobW9kZSAmIDQpICYmIHR5cGVvZiB2YWx1ZSA9PT0gJ29iamVjdCcgJiYgdmFsdWUgJiYgdmFsdWUuX19lc01vZHVsZSkgcmV0dXJuIHZhbHVlO1xuIFx0XHR2YXIgbnMgPSBPYmplY3QuY3JlYXRlKG51bGwpO1xuIFx0XHRfX3dlYnBhY2tfcmVxdWlyZV9fLnIobnMpO1xuIFx0XHRPYmplY3QuZGVmaW5lUHJvcGVydHkobnMsICdkZWZhdWx0JywgeyBlbnVtZXJhYmxlOiB0cnVlLCB2YWx1ZTogdmFsdWUgfSk7XG4gXHRcdGlmKG1vZGUgJiAyICYmIHR5cGVvZiB2YWx1ZSAhPSAnc3RyaW5nJykgZm9yKHZhciBrZXkgaW4gdmFsdWUpIF9fd2VicGFja19yZXF1aXJlX18uZChucywga2V5LCBmdW5jdGlvbihrZXkpIHsgcmV0dXJuIHZhbHVlW2tleV07IH0uYmluZChudWxsLCBrZXkpKTtcbiBcdFx0cmV0dXJuIG5zO1xuIFx0fTtcblxuIFx0Ly8gZ2V0RGVmYXVsdEV4cG9ydCBmdW5jdGlvbiBmb3IgY29tcGF0aWJpbGl0eSB3aXRoIG5vbi1oYXJtb255IG1vZHVsZXNcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubiA9IGZ1bmN0aW9uKG1vZHVsZSkge1xuIFx0XHR2YXIgZ2V0dGVyID0gbW9kdWxlICYmIG1vZHVsZS5fX2VzTW9kdWxlID9cbiBcdFx0XHRmdW5jdGlvbiBnZXREZWZhdWx0KCkgeyByZXR1cm4gbW9kdWxlWydkZWZhdWx0J107IH0gOlxuIFx0XHRcdGZ1bmN0aW9uIGdldE1vZHVsZUV4cG9ydHMoKSB7IHJldHVybiBtb2R1bGU7IH07XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18uZChnZXR0ZXIsICdhJywgZ2V0dGVyKTtcbiBcdFx0cmV0dXJuIGdldHRlcjtcbiBcdH07XG5cbiBcdC8vIE9iamVjdC5wcm90b3R5cGUuaGFzT3duUHJvcGVydHkuY2FsbFxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5vID0gZnVuY3Rpb24ob2JqZWN0LCBwcm9wZXJ0eSkgeyByZXR1cm4gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsKG9iamVjdCwgcHJvcGVydHkpOyB9O1xuXG4gXHQvLyBfX3dlYnBhY2tfcHVibGljX3BhdGhfX1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5wID0gXCJcIjtcblxuXG4gXHQvLyBMb2FkIGVudHJ5IG1vZHVsZSBhbmQgcmV0dXJuIGV4cG9ydHNcbiBcdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKF9fd2VicGFja19yZXF1aXJlX18ucyA9IFwiLi9hcHBsaWNhdGlvbi9pbmRleC5qc1wiKTtcbiIsIi8vIGltcG9ydCBCZWFjaFBhcnR5IGZyb20gJy4vdGFzazEvY29vbGVkaHVtYW4nO1xuaW1wb3J0IHsgQXBwbGljYXRpb24gfSBmcm9tICcuL3Rhc2syJztcblxuLy8gQmVhY2hQYXJ0eSgpO1xuXG5jb25zdCBEZW1vID0gKCkgPT4ge1xuICAgIGNvbnN0IE5vdGlmaWVyQXBwID0gbmV3IEFwcGxpY2F0aW9uKFxuICAgICAgW1xuICAgICAgICB7IGNvbnRhY3Q6ICcwOTg3NjU0NCcsICAgICAgIHR5cGU6ICdzbXMnLCAgICAgICAgaW1hZ2U6ICdzbXMuc3ZnJyB9LFxuICAgICAgICB7IGNvbnRhY3Q6ICcwOTg3OTg3NCcsICAgICAgIHR5cGU6ICdzbXMnLCAgICAgICAgaW1hZ2U6ICdzbXMuc3ZnJyB9LFxuICAgICAgICB7IGNvbnRhY3Q6ICdmZ3ZAZ2JmLmNvbScsICAgIHR5cGU6ICdnbWFpbCcsICAgICAgaW1hZ2U6ICdnbWFpbC5zdmcnIH0sXG4gICAgICAgIHsgY29udGFjdDogJzA5ODc2NTQ0JywgICAgICAgdHlwZTogJ3RlbGVncmFtJywgICBpbWFnZTogJ3RlbGVncmFtLnN2ZycgfSxcbiAgICAgICAgeyBjb250YWN0OiAnMDk4NzY1NDQnLCAgICAgICB0eXBlOiAndmliZXInLCAgICAgIGltYWdlOiAndmliZXIuc3ZnJyB9LFxuICAgICAgXVxuICAgICk7XG4gICAgTm90aWZpZXJBcHAucmVuZGVyKCk7XG4gIFxuICAgIGNvbnNvbGUubG9nKCBOb3RpZmllckFwcCApO1xuICBcbiAgICAvLyBjb25zdCBOb3RBcHAgPSBuZXcgQXBwbGljYXRpb24oXG4gICAgLy8gICBbXG4gICAgLy8gICAgIHtuYW1lOiAnbWFpbCcsIGltYWdlOiAnaW1hZ2VzL2dtYWlsLnN2Zyd9LFxuICAgIC8vICAgICB7bmFtZTogJ3RlbGVncmFtJywgaW1hZ2U6ICdpbWFnZXMvdGVsZWdyYW0uc3ZnJ30sXG4gICAgLy8gICAgIHtuYW1lOiAnc2xhY2snLCBpbWFnZTogJ2ltYWdlcy9zbGFjay5zdmcnfSxcbiAgICAvLyAgIF1cbiAgICAvLyApO1xuICAgIC8vIE5vdEFwcC5jcmVhdGVJbnRlcmZhY2UoKTtcbiAgXG4gIH1cblxuICBEZW1vKCk7IiwiaW1wb3J0IHsgTm90aWZpZXIgfSBmcm9tICcuL25vdGlmaWVyJztcblxuZXhwb3J0IGNsYXNzIEFwcGxpY2F0aW9uIHtcbiAgICBjb25zdHJ1Y3Rvciggbm90aWZpZXJzICkge1xuICAgICAgdGhpcy5ub3RlVGFyZ2V0cyA9IG5vdGlmaWVycztcbiAgICAgIHRoaXMubm90aWZpZXJzID0gbmV3IE5vdGlmaWVyKCB0aGlzLm5vdGVUYXJnZXRzICk7XG4gICAgICB0aGlzLnJlbmRlciA9IHRoaXMucmVuZGVyLmJpbmQodGhpcyk7XG4gICAgfVxuXG4gICAgcmVuZGVyKCkge1xuICAgICAgY29uc3Qgcm9vdCA9IGRvY3VtZW50LmdldEVsZW1lbnRCeUlkKCdyb290Jyk7XG4gICAgICBjb25zdCBBcHBOb2RlID0gZG9jdW1lbnQuY3JlYXRlRWxlbWVudCgnc2VjdGlvbicpO1xuICBcbiAgICAgIEFwcE5vZGUuY2xhc3NOYW1lID0gJ25vdGlmZXJfYXBwJztcbiAgICAgIEFwcE5vZGUuaW5uZXJIVE1MID1cbiAgICAgIGBcbiAgICAgICAgPGRpdiBjbGFzcz1cIm5vdGlmZXJfYXBwLS1jb250YWluZXJcIj5cbiAgICAgICAgICA8aGVhZGVyPlxuICAgICAgICAgICAgPGlucHV0IGNsYXNzPVwibm90aWZpZXJfX21lc3NhbmdlclwiIHR5cGU9XCJ0ZXh0XCIvPlxuICAgICAgICAgICAgPGJ1dHRvbiBjbGFzcz1cIm5vdGlmaWVyX19zZW5kXCI+U2VuZCBNZXNzYWdlPC9idXR0b24+XG4gICAgICAgICAgPC9oZWFkZXI+XG4gICAgICAgICAgPGRpdiBjbGFzcz1cIm5vdGlmaWVyX19jb250YWluZXJcIj5cbiAgICAgICAgICAke1xuICAgICAgICAgICAgdGhpcy5ub3RlVGFyZ2V0cy5tYXAoIGl0ZW0gPT5cbiAgICAgICAgICAgICAgYFxuICAgICAgICAgICAgICA8ZGl2IGNsYXNzPVwibm90aWZpZXJfX2l0ZW1cIiBkYXRhLXNsdWc9XCIke2l0ZW0udHlwZX0tLSR7aXRlbS5jb250YWN0fVwiPlxuICAgICAgICAgICAgICAgIDxoZWFkZXIgY2xhc3M9XCJub3RpZmllcl9faGVhZGVyXCI+XG4gICAgICAgICAgICAgICAgICA8aW1nIHdpZHRoPVwiMjVcIiBzcmM9XCJwdWJsaWMvaW1hZ2VzLyR7aXRlbS50eXBlfS5zdmdcIi8+XG4gICAgICAgICAgICAgICAgICA8c3Bhbj4ke2l0ZW0uY29udGFjdH08L3NwYW4+XG4gICAgICAgICAgICAgICAgPC9oZWFkZXI+XG4gICAgICAgICAgICAgICAgPHVsIGNsYXNzPVwibm90aWZpZXJfX21lc3NhZ2VzXCI+PC91bD5cbiAgICAgICAgICAgICAgPC9kaXY+XG4gICAgICAgICAgICAgIGApLmpvaW4oJycpXG4gICAgICAgICAgfVxuICAgICAgICAgIDwvZGl2PlxuICAgICAgICA8L2Rpdj5cbiAgICAgIGA7XG4gICAgICBjb25zdCBjb250YWluZXIgPSBBcHBOb2RlLnF1ZXJ5U2VsZWN0b3IoJy5ub3RpZmllcl9fY29udGFpbmVyJyk7XG4gICAgICBjb25zdCBidG4gPSBBcHBOb2RlLnF1ZXJ5U2VsZWN0b3IoJy5ub3RpZmllcl9fc2VuZCcpO1xuICAgICAgY29uc3QgaW5wdXQgPSBBcHBOb2RlLnF1ZXJ5U2VsZWN0b3IoJy5ub3RpZmllcl9fbWVzc2FuZ2VyJyk7XG4gICAgICBidG4uYWRkRXZlbnRMaXN0ZW5lcignY2xpY2snLCAoKSA9PiB7XG4gICAgICAgIGxldCB2YWx1ZSA9IGlucHV0LnZhbHVlO1xuICAgICAgICB0aGlzLm5vdGlmaWVycy5zZW5kTmV3cyggdmFsdWUsIGNvbnRhaW5lciApO1xuICAgICAgfSk7XG4gIFxuICAgICAgdGhpcy5ub2RlID0gQXBwTm9kZTtcbiAgICAgIHJvb3QuYXBwZW5kQ2hpbGQoQXBwTm9kZSk7XG4gICAgfVxufVxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG5cblxuXG4iLCJleHBvcnQgY2xhc3MgTWFzc2VuZ2VyIHtcbiAgICBjb25zdHJ1Y3RvciggeyBjb250YWN0IH0gKSB7XG4gICAgICAgIHRoaXMuY29udGFjdCA9IGNvbnRhY3Q7XG4gICAgfVxuICAgIHNlbmQoYmFzZU5vZGUsIG1zZywgY29udGFjdCkge1xuICAgICAgICBjb25zdCBwYXJlbnQgPSBiYXNlTm9kZS5xdWVyeVNlbGVjdG9yKGAubm90aWZpZXJfX2l0ZW1bZGF0YS1zbHVnPVwiJHtjb250YWN0fVwiXWApOyBcbiAgICAgICAgY29uc3QgdGFyZ2V0ID0gcGFyZW50LnF1ZXJ5U2VsZWN0b3IoJy5ub3RpZmllcl9fbWVzc2FnZXMnKTtcbiAgICAgICAgdGFyZ2V0LmlubmVySFRNTCArPSBgPGxpPiR7bXNnfTwvbGk+YDtcbiAgICB9XG59XG5cbmV4cG9ydCBjbGFzcyBUZWxlZ3JhbSBleHRlbmRzIE1hc3NlbmdlciB7XG4gICAgc2VuZChiYXNlTm9kZSwgbXNnKSB7XG4gICAgICAgIGxldCBjb250ID0gYHRlbGVncmFtLS0ke3RoaXMuY29udGFjdH1gXG4gICAgICAgIHN1cGVyLnNlbmQoYmFzZU5vZGUsIG1zZywgY29udCk7XG4gICAgfVxufVxuXG5leHBvcnQgY2xhc3MgVmliZXIgZXh0ZW5kcyBNYXNzZW5nZXIge1xuICAgIHNlbmQoYmFzZU5vZGUsIG1zZykge1xuICAgICAgICBsZXQgY29udCA9IGB2aWJlci0tJHt0aGlzLmNvbnRhY3R9YFxuICAgICAgICBzdXBlci5zZW5kKGJhc2VOb2RlLCBtc2csIGNvbnQpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIFNtcyBleHRlbmRzIE1hc3NlbmdlciB7XG4gICAgc2VuZChiYXNlTm9kZSwgbXNnKSB7XG4gICAgICAgIGxldCBjb250ID0gYHNtcy0tJHt0aGlzLmNvbnRhY3R9YFxuICAgICAgICBzdXBlci5zZW5kKGJhc2VOb2RlLCBtc2csIGNvbnQpO1xuICAgIH1cbn1cblxuZXhwb3J0IGNsYXNzIEdtYWlsIGV4dGVuZHMgTWFzc2VuZ2VyIHtcbiAgICBzZW5kKGJhc2VOb2RlLCBtc2cpIHtcbiAgICAgICAgbGV0IGNvbnQgPSBgZ21haWwtLSR7dGhpcy5jb250YWN0fWBcbiAgICAgICAgc3VwZXIuc2VuZChiYXNlTm9kZSwgbXNnLCBjb250KTtcbiAgICB9XG59IiwiaW1wb3J0IHsgVGVsZWdyYW0sIFZpYmVyLCBTbXMsIEdtYWlsIH0gZnJvbSAnLi9tYXNzZW5nZXJzJztcblxuZXhwb3J0IGNsYXNzIE5vdGlmaWVyIHtcbiAgICBjb25zdHJ1Y3Rvciggc3Vic2NyaWJlcnMgKSB7XG4gICAgICAgIGxldCBjbGllbnRzID0gc3Vic2NyaWJlcnMubWFwICggb25lID0+IHtcbiAgICAgICAgICAgIGlmIChvbmUudHlwZSA9PT0gJ3RlbGVncmFtJykgcmV0dXJuIG5ldyBUZWxlZ3JhbShvbmUpO1xuICAgICAgICAgICAgZWxzZSBpZiAob25lLnR5cGUgPT09ICd2aWJlcicpIHJldHVybiBuZXcgVmliZXIob25lKTtcbiAgICAgICAgICAgIGVsc2UgaWYgKG9uZS50eXBlID09PSAnc21zJykgcmV0dXJuIG5ldyBTbXMob25lKTtcbiAgICAgICAgICAgIGVsc2UgaWYgKG9uZS50eXBlID09PSAnZ21haWwnKSByZXR1cm4gbmV3IEdtYWlsKG9uZSk7XG4gICAgICAgICAgICBlbHNlIHJldHVybiBudWxsO1xuICAgICAgICB9ICk7XG4gICAgICAgIHRoaXMuY2xpZW50cyA9IGNsaWVudHM7XG4gICAgfTtcbiAgICBzZW5kTmV3cyhtc2csIG5vZGEpIHtcbiAgICAgICAgdGhpcy5jbGllbnRzLm1hcChvbmUgPT4geyBvbmUuc2VuZCggbm9kYSwgbXNnICkgfSApO1xuICAgIH07XG59Il0sInNvdXJjZVJvb3QiOiIifQ==