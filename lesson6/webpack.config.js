const path = require('path');
// const HtmlWebpackPlugin = require('html-webpack-plugin');
// const { CleanWebpackPlugin } = require('clean-webpack-plugin');

module.exports = {
    mode: 'development',
    entry: {
      app: './application/index.js',
    //   print: './src/print.js'
    },
    devtool: 'inline-source-map',
    devServer: {
        contentBase: '.'
    },
    // plugins: [
    //   new CleanWebpackPlugin(),
    //   new HtmlWebpackPlugin({
    //     title: 'Development'
    //   })
    // ],
    output: {
      filename: '[name].bundle.js',
      path: path.resolve(__dirname, 'public')
    }
};